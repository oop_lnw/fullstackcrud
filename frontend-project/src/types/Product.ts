export default interface Product {
  id?: number;
  name: string;
  price: number;
  createAt?: Date;
  updateAt?: Date;
  deletedAt?: Date;
}
